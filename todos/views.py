from django.shortcuts import render
from todos.models import TodoList


# Create your views here.
def todo_list_list(request):
    todolist_list = TodoList.objects.all()
    context = {"todolist_list": todolist_list}
    return render(request, "todos/todos.html", context)
